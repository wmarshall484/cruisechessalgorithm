#A solution to Cruise's single-knight chess board problem.

The solution formulates to board as a graph, and uses dijkstra's
algorithm to find the shortest path. For board sizes that scale
to sizes larger than 32x32, the A* algorithm may be more
appropriate, using the as-the-crow-flies distance as a hueristic
(although this would only be an approximation if teleporters are
included in the graph).
